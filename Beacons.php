<?php

namespace App\Controllers;

class Beacons  extends BaseController
{
	const valid_modes = ['UNFILTERED', 
						 'NO_WORK_GAPS', 
						 'NO_WORK_AND_REST_GAPS'];

	private function applyFilter(&$bdata, $interval_threshold, $interval_type) {
		do {
			$were_changes = false;
			$stat = $this->calcIntervals($bdata);
			foreach ($bdata as $beacon_id => &$beacon_minutes) {
				foreach ($beacon_minutes as $timestamp => &$signal) {
					if ($signal == $interval_type && $stat[$beacon_id][$timestamp] < $interval_threshold) {
						$signal ^= 1;
						$were_changes = true;
					}
				}
			}
		}
		while ($were_changes);
	}

	private function calcIntervals($bdata) {
		$intervals = [];
		foreach ($bdata as $b_id => $minutes) {
			$tmpIntCounts = [];
			$signalValues = array_values($minutes);
			$signalTimes = array_keys($minutes);
			$lastIx = 0;
			$lastVal = $signalValues[$lastIx];
			$lastMinCount = 1;
			// aggregate interval data
			while ($lastIx < count($signalValues)) {
				for ($i=$lastIx+1; array_key_exists($i, $signalValues) && $signalValues[$i] == $lastVal; $i++) { 
					$lastMinCount++;
				}
				// save interval size
				for ($j=0; $j < $lastMinCount; $j++) { 
					$tmpIntCounts[] = $lastMinCount;
				}
				// reseting last interval data
				$lastIx = $i;
				$lastVal = $signalValues[$lastIx];
				$lastMinCount = 1;
			}	
			// create statistics array
			$intervals[$b_id] = array_combine($signalTimes, $tmpIntCounts);
		}
		return $intervals;
	} 

	public function fetch(string $mode, $date)
	{
		if (!in_array($mode, self::valid_modes)) {
			throw new \RuntimeException("Pass valid filter mode !");
		}

        $db = db_connect();
		$cmd = $db->query("SET cte_max_recursion_depth = 4294967295;");
		if ($cmd->connID->error) {
			throw new \RuntimeException("Unable to set recursion depth ! :: {$cmd->connID->error}");
		}

		// optimizavimas,- jei sugeneruoti plain rezai - skipinam baze, nes ilgai SQL uztrunka
		define('PLAIN_DATA_FILE', 'UNFILTERED.json');
		if (file_exists(PLAIN_DATA_FILE) && filesize(PLAIN_DATA_FILE) > 0) {
			$out = json_decode(file_get_contents(PLAIN_DATA_FILE), true);
		}
		else {
			$beacons = $db->query("SELECT DISTINCT beacon_id FROM beacons where date(date) = '{$date}' ORDER BY 1")->getResult('array');
			$out = [];
			// kiekvienam beaconui traukiam signalu info
			foreach($beacons as $beacon) {
					$main_sql = "
								WITH RECURSIVE day_minutes AS (
									SELECT 1 AS level
									UNION ALL
									SELECT level + 1 AS value
									FROM day_minutes
									WHERE day_minutes.level < 1439
								)
								select distinct
										daytime, exists(select * 
												from beacons b 
												where b.beacon_id = {$beacon['beacon_id']} and 
														DATE_FORMAT(b.date, '%Y-%m-%d %H:%i:00') = generatedmins.daytime
												) detected
								from
								(
								SELECT level, DATE_ADD('{$date}', INTERVAL level minute) as daytime
								FROM day_minutes
								) generatedmins
								order by 1
								;
								";
					$basic_stat = $db->query($main_sql)->getResult('array');
					$out[$beacon['beacon_id']] = array_combine(array_column($basic_stat, "daytime"), array_column($basic_stat, "detected"));
			}
		}
		
		if ($mode == 'UNFILTERED') {
		   $jsonres = $out;
		} elseif ($mode == 'NO_WORK_GAPS') {
			$this->applyFilter($out, 15, 0);
			$jsonres = $out;
		} elseif ($mode == 'NO_WORK_AND_REST_GAPS') {
			$this->applyFilter($out, 15, 0);
			$this->applyFilter($out, 10, 1);
			$jsonres = $out;
		}

		// rezultatu formatavimas
		$jsonres = json_encode($jsonres, JSON_PRETTY_PRINT);
        echo $jsonres;
	}
}
